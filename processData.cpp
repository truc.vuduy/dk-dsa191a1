/* Created by Nguyen Duc Dung on 2019-09-03.
 * =========================================================================================
 * Name        : processData.cpp
 * Author      : Duc Dung Nguyen
 * Email       : nddung@hcmut.edu.vn
 * Copyright   : Faculty of Computer Science and Engineering - HCMUT
 * Description : Implementation of main features in the assignment
 * Course      : Data Structure and Algorithms - Fall 2019
 * =========================================================================================
 */

#include "processData.h"

/* TODO: You can implement methods, functions that support your data structures here.
 * */
using namespace std;

int* pOutput;

void Initialization() {
    // If you use global variables, please initialize them explicitly in this function.
}

void Finalization() {
    // Release your data before exiting
}

int reqFC_id = -1;
void findCityByName(TCity &city) {
	if (id != -1 && city.name == city_name) {
		city_id = city.id;
	}
}

void requestFC(TDataset* pData, void* &pOutput, int &N, city_name) {
	city_id = -1;
	
	L1List<TCity> pCities = pDataset->cities;
	city_name = "Ho Chi Minh";
	
	pCities->traverse(findCityByName);
	int* pRs = new int[1];
	pRs[0] = id;
	N = 1;
	pOutput = (void*) pRs;
}

void ProcessRequest(const char* pRequest, void* pData, void* &pOutput, int &N) {
    // TODO: Implement this function for processing a request
    // NOTE: You can add other functions to support this main process.
    //       pData is a pointer to a data structure that manages the dataset
    //       pOutput is a pointer reference. It is set to nullptr and student must allocate data for it in order to save the required output
    //       N is the size of output, must be a non-negative number
    
//    class TDataset {
//    // This class can be a container that help you manage your tables
//    L1List<TCity> *cities;
//    L1List<TLine> *lines;
//    L1List<TStation> *stations;
//    L1List<TStationLine> *station_lines;
//    L1List<TTrack> *tracks;	
	
//	struct TCity {
//	    // The structure to store city information
//	    string id;
//	    string name;

	TDataset* pDataset = (TDataset*)pData;
}


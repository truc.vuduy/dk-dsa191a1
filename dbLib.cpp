/* Created by Nguyen Duc Dung on 2019-09-03.
 * =========================================================================================
 * Name        : dbLib.cpp
 * Author      : Duc Dung Nguyen
 * Email       : nddung@hcmut.edu.vn
 * Copyright   : Faculty of Computer Science and Engineering - HCMUT
 * Description : The data structure library for Assignment 1
 * Course      : Data Structure and Algorithms - Fall 2019
 * =========================================================================================
 */

#include "dbLib.h"

/* TODO: You can implement methods, functions that support your data structures here.
 * */
void printString(string& s) { cout << "`" << s << "`"; }
void printGeo(string& s) {
	cout << "\t\t";
	printString(s);
	cout << endl;
}


TTrack *tTrackParser(string &line) {
	stringstream ss(line);
	string value, _temp;
	string empty = "";

    string id;
	L1List<string> *geos = new L1List<string>();
	string city_id;

	// id
	getline(ss, id, ',');

	// geos
	if (line.find("LINESTRING(")) {
		cout << endl << "LINESTRING(" << endl;
		getline(ss, value, '(');
		getline(ss, value, ')');

		stringstream ssGeos(value);
		while (getline(ssGeos, _temp, ',')) {
			if (line.compare(empty) == 0) break;
			geos->push_back(_temp);
		}

		getline(ss, value, ',');
	} else {
		getline(ss, value, ',');
	}

	// city_id
	for (size_t c = 0; c < 4; c++) getline(ss, value, ',');
	getline(ss, city_id, ',');
	
	return new TTrack(id, geos, city_id);
}

void LoadData(void* &) {

	L1List<TTrack> *ll = new L1List<TTrack>();
	TTrack *track = nullptr;

	ifstream csvFile("_private/test.csv");
//	ifstream csvFile("tracks.csv");
	string line;
	string empty = "";

	getline(csvFile, line);
	while(getline(csvFile, line)) {
		if (line.compare(empty) == 0) break;
	
		track = tTrackParser(line);
		ll->push_back(*track);
		cout << endl << "input line = " << line;
		cout << endl << endl;
		track->print();
		
		cout << endl;	
	}

}

void ReleaseData(void* &) {}



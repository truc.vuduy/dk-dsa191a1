#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <cstring>
#include <sstream>
#include <math.h>

#include "dsaLib.h"
#include "dbLib.h"
#include "processData.h"

using namespace std;

void printData(void *pData);

int main() {
    Initialization();

    void*   pData = nullptr;
    void*   pOutput = nullptr;
    int     N;

    LoadData(pData);
//    assert(pData != nullptr);
//    cout << fixed << setprecision(8);
//    string req;
//    while (true) {
//        req = "";
//        getline(cin, req);
//        if (cin.bad()) {
//            cin.clear();
//            cin.ignore(1024, '\n');
//            continue;
//        }
//        if (req == "Exit") {
//            break;
//        }
//        ProcessRequest(req.data(), pData, pOutput, N);
//        PrintReqOutput<int>(req.data(), (int*)pOutput, N);
//        delete [] (int*)pOutput;
//        pOutput = nullptr;
//    }

//	TDataset* pDataset = (TDataset*)(pData);

//	printData(pData);

    ReleaseData(pData);
    Finalization();
    return 0;
}


void printLL(L1List<TTrack> *list) {
	//
}

void printData(void *pData) {
	//
}


void printInt(int& i) {
	cout << i << " ";
}

void printLL(L1List<int> *list) {
	cout << endl << "LL(_size:" << list->getSize() << ")  " << endl;
	list->traverse(printInt);
	cout << endl;
}


int main2() {
	int a[] = { 1, 2, 3, 4 };

    L1List<int> *ll = new L1List<int>();

    ll->push_back(a[0]);
    ll->push_back(a[1]);
    ll->push_back(a[2]);
    ll->push_back(a[3]);

    printLL(ll);

    int d = 10;
    ll->insertHead(d);
    d = 7;
    ll->insertHead(d);

    printLL(ll);

    ll->removeHead();

    printLL(ll);

	ll->removeLast();

    printLL(ll);

	d = 99;
	ll->push_back(d);
    d = 11;
    ll->insertHead(d);

    printLL(ll);

    return 0;
}


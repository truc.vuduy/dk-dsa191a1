/* Created by Nguyen Duc Dung on 2019-09-03.
 * =========================================================================================
 * Name        : dbLib.h
 * Author      : Duc Dung Nguyen
 * Email       : nddung@hcmut.edu.vn
 * Copyright   : Faculty of Computer Science and Engineering - HCMUT
 * Description : The data structure library for Assignment 1
 * Course      : Data Structure and Algorithms - Fall 2019
 * =========================================================================================
 */

#ifndef DSA191_A1_DBLIB_H
#define DSA191_A1_DBLIB_H

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <cstring>
#include <sstream>
#include <math.h>

#include "dsaLib.h"

/* TODO: Please design your data structure carefully so that you can work with the given dataset
 *       in this assignment. The below structures are just some suggestions.
 */
void printString(string& s);
void printGeo(string& s);


struct TCity {
    // The structure to store city information
    string id;
    string name;

    TCity(string i, string n) : id(i), name(n) {}

    void print() {
        cout << "TCity : " << endl;
        cout << "\tid : "; printString(id); cout << endl;
        cout << "\tname : "; printString(name); cout << endl;
    }
};

struct TLine {
    // The structure to store line information
    string id;
    string city_id;

    TLine(string i, string c) : id(i), city_id(c) {}
    
    void print() {
        cout << "TLine : " << endl;
        cout << "\tid : "; printString(id); cout << endl;
        cout << "\tcity_id : "; printString(city_id); cout << endl;
    }
};

struct TStation {
    // The structure to store line information
    string id;
    string name;
    string geo;
    string city_id;

    TStation(string i, string n, string g, string c) : id(i), name(n), geo(g), city_id(c) {}
    
    void print() {
        cout << "TStation : " << endl;
        cout << "\tid : "; printString(id); cout << endl;
        cout << "\tname : "; printString(name); cout << endl;
        cout << "\tgeo : "; printString(geo); cout << endl;
        cout << "\tcity_id : "; printString(city_id); cout << endl;
    }
};

struct TStationLine {
    // The structure to store line information
    string station_id;
    string line_id;

    TStationLine(string s, string l) : station_id(s), line_id(l) {}
    
    void print() {
        cout << "TStationLine : " << endl;
        cout << "\tstation_id : "; printString(station_id); cout << endl;
        cout << "\tline_id : "; printString(line_id); cout << endl;
    }
};

struct TTrack {
    // The structure to store track information
    string id;
    L1List<string> *geos;
    string city_id;
    
    TTrack(string i, L1List<string> *g, string c) : id(i), geos(g), city_id(c) {}
    ~TTrack() { if (geos) delete geos; }
    
    void print() {
        cout << "TTrack : " << endl;
        cout << "\tid : "; printString(id); cout << endl;
        cout << "\tcity_id : "; printString(city_id); cout << endl;
        cout << "\tGeos : " << endl;
        geos->traverse(printGeo);
        cout << endl;
    }
};

class TDataset {
    // This class can be a container that help you manage your tables
    L1List<TCity> *cities;
    L1List<TLine> *lines;
    L1List<TStation> *stations;
    L1List<TStationLine> *station_lines;
    L1List<TTrack> *tracks;
    
    TDataset() {
        cities = new L1List<TCity>();
        lines = new L1List<TLine>();
        stations = new L1List<TStation>();
        station_lines = new L1List<TStationLine>();
        tracks = new L1List<TTrack>();
    }

    ~TDataset() {
//        delete cities;
//        delete lines;
//        delete stations;
//        delete station_lines;
//        delete tracks;
    }
};

// Please add more or modify as needed

void LoadData(void* &);
void ReleaseData(void* &);

#endif //DSA191_A1_DBLIB_H

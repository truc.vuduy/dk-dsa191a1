#include "dsaLib.h"

template <class T>
L1List<T>::~L1List() {
	L1Item<T> *p = _pHead;
	L1Item<T> *pNext;
	while (p) {
		pNext = p->pNext;
		p->pNext = nullptr;
		delete p;
		p = pNext;
	}
}


template <class T>
void L1List<T>::clean() {}


template <class T>
T& L1List<T>::at(int i) {
    // give the reference to the element i-th in the list
    L1Item<T> *p = _pHead;
    for (size_t c = 0;  c < i - 1;  c++) {
        p = p->pNext;
    }
    return p->data;
}

template <class T>
T& L1List<T>::operator[](int i) {
    // give the reference to the element i-th in the list
    return at(i);
}


template <class T>
bool L1List<T>::find(T& a, int& idx){
    // find an element similar to a in the list. Set the found index to idx, set idx to -1 if failed. Return true if success.
}

template <class T>
int L1List<T>::insert(int i, T& a) {
    // insert an element into the list at location i. Return 0 if success, -1 otherwise
}

template <class T>
int L1List<T>::remove(int i) {
    // remove an element at position i in the list. Return 0 if success, -1 otherwise.
}


template <class T>
void L1List<T>::reverse() {
    //
}

